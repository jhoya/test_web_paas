#!/usr/bin/python3

#print("Content-Type: text/html;charset=utf-8\n")
#print("Content-Type: application/json;\n")
import subprocess
import cgi
import sys
#print(sys.version)

#set_rel = subprocess.run(["source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh > /dev/null 2>&1"], shell=True)
#set_rel = subprocess.run(["source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh")

#import cgitb
import json

sys.path.insert(0,'/cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc8-opt/lib/python3.7/site-packages')
import mysql.connector
from mysql.connector import Error
#
sys.path.insert(0,'/afs/cern.ch/user/j/jhoya/www/otp_checks/withMySQL')
from config import DB_CONFIG
#
## MySQL connection configuration
config = DB_CONFIG
#
#
# Function to update the boolean value in the MySQL database
def update_value(name, column):
    #print(name, column)
    try:
        # Connect to the MySQL database
        conn = mysql.connector.connect(**config)
        cursor = conn.cursor(buffered=True)

        # Retrieve the current value from the shifters table
        query = f"SELECT {column} FROM shifters WHERE name = %s;"
        cursor.execute(query, (name,))
        conn.commit()

        row = cursor.fetchone()

        if row is not None:
            current_value = row[0]
            #logging.debug(f"Current value: {current_value}")
            #print(f"Current value: {current_value}")
            new_value = 0 if current_value else 1
            #logging.debug(f"New value: {new_value}")
            #print(f"New value: {new_value}")

            # Update the value in the shifters table
            update_query = f"UPDATE shifters SET {column} = %s WHERE name = %s LIMIT 1;"
            cursor.execute(update_query, (new_value, name))
            conn.commit()
            #print(cursor.rowcount, "record(s) affected") 
        else:
            new_value = "NONE"

        #logging.debug(f"Final value: {new_value}")
        #print(f"Final value: {new_value}")
        return new_value

    except Error as e:
        logging.error("Error while updating value in MySQL: %s", str(e))
        return None

    finally:
        if conn.is_connected():
            cursor.close()
            conn.close()


# Main CGI script execution
if __name__ == '__main__':

    # Enable detailed error messages in the browser
    #cgitb.enable()
    #cgitb.enable(display=1, logdir="/afs/cern.ch/user/j/jhoya/www/otp_checks/withMySQL/log/")

    # Retrieve the parameters
    query_string = cgi.FieldStorage()
    name = query_string.getvalue('name')
    column = query_string.getvalue('column')

    # Update the boolean value and get the new value
    new_value = update_value(name, column)
    #new_value = name

    # Prepare the response
    response = {'value': new_value}

    #regen_web = "/afs/cern.ch/user/j/jhoya/www/otp_checks/withMySQL/generate_index.py"
    #subprocess.run(["python3", regen_web], shell=True)
    #regen_web.wait()

    # Return the new value as JSON response
    print("Content-Type: application/json")
    print()
    print(json.dumps(response))
