#!/usr/bin/python3

import mysql.connector
from mysql.connector import Error
#import cgitb
#cgitb.enable()
import json

from config import DB_CONFIG
# MySQL connection configuration
config = DB_CONFIG

# Function to fetch data from the MySQL database
def get_data():
    try:
        conn = mysql.connector.connect(**config)
        cursor = conn.cursor(buffered=True)

        # Retrieve data from the names table
        query = "SELECT name, tra, rc_sir, trig_sir FROM shifters"
        cursor.execute(query)
        data = cursor.fetchall()

        return data

    except Error as e:
        print("Error while connecting to MySQL", e)

    finally:
        if conn.is_connected():
            cursor.close()
            conn.close()

# Function to generate the table rows HTML
def generate_table_rows(data):
    table_rows = ""
    for row in data:
        name = row[0]
        tra = "Yes" if row[1] else "No"
        rc_sir = "Yes" if row[2] else "No"
        trig_sir = "Yes" if row[3] else "No"

        table_rows += f"""
            <tr>
                <td>{name}</td>
                <td class="clickable" onclick="toggleValue(this, '{name}', 'tra')">{tra}</td>
                <td class="clickable" onclick="toggleValue(this, '{name}', 'rc_sir')">{rc_sir}</td>
                <td class="clickable" onclick="toggleValue(this, '{name}', 'trig_sir')">{trig_sir}</td>
            </tr>
        """

    return table_rows

# Main script execution
if __name__ == '__main__':

    # Retrieve data from the database
    data = get_data()

    # Generate the table rows HTML
    table_rows = generate_table_rows(data)


    my_styles = """
        <style>
            .clickable {
                text-decoration: underline;
                color: blue;
                cursor: pointer;
            }
        </style>
    """

    javascript_code = """
            <script>
                function toggleValue(cell, name, column) {

                    //document.getElementById("p1").innerHTML = name;
                    //$(\'#report\').one(\'click\', \'td\', function(){
                    //    var this$   = $(this),
                    //    _status = !!this$.data(\'status\')
                    //    this$.html(_status ? \'No\' : \'Yes\').data(\'status\', !_status);
                    //});
                    var cvalue = cell.innerHTML;

                    // Send AJAX request to update the value
                    $.ajax({
                        url: '/jhoya/cgi-bin/toggle.cgi',
                        type: 'POST',
                        data: {
                            'name': name,
                            'column': column
                        },
                        success: function(response) {
                            // Update the table cell with the new value
                            //document.getElementById("p2").innerHTML = response.value;
                            new_value = response.value == 1 ? "Yes" : "No";
                            cell.innerHTML = new_value;
                            //cell.innerHTML = response.value;
                            //$('#p2').text(response.value);
                        },
                        error: function(xhr, status, error) {
                            //console.log('Error:', error);                           
                        }
                    });
                }
            </script>
    """


    # Create the HTML content with the table
    html_content = f"""
        <html>
        <head>
            <title>DB Table</title>
            {my_styles}
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
            {javascript_code}
        </head>
        <body>
            <table id="report">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>TRA</th>
                        <th>RC_SIR</th>
                        <th>TRIG_SIR</th>
                    </tr>
                </thead>
                <tbody>
                    {table_rows}
                </tbody>
            </table>
        </body>
        </html>
    """

    # Save the HTML content to index.html
    with open('index.html', 'w') as file:
        file.write(html_content)

    # Print the webpage content
    #print("Content-Type: text/html")
    #print()
    #print(html_content)

